# petstore-design-first

Sample Petstore API, illustrating the implementation of the "Design First" concept.

All explanations are available here : https://blog.maxds.fr/design-first-api-with-fastify

## Getting started

You can clone and run this Node.JS application by executing following commands :

```shell
> git clone git@gitlab.com:gasouch/petstore-design-first.git

> cd petstore-design-first

> npm i

> npm run start
```
