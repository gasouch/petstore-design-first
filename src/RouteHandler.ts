import type {FastifyReply, FastifyRequest} from 'fastify'
import Pet from "./domain/Pet";
import {dogs, fishes} from "./domain/Category";

class RouteHandler {

    petstore: Map<number, Pet> = new Map(
        [
            [1, {id: 1, name: "Labrador Retriever", category: dogs}],
            [2, {id: 2, name: "Goldfish", category: fishes}]
        ]
    )

    addPet = async (req: FastifyRequest, reply: FastifyReply) => {
        const petstore: Map<number, Pet> = new Map()

        const pet: any = req.body as Pet
        console.log("add new pet : " + pet)
        try {
            if (this.petstore.has(pet.id)) {
                return reply
                    .code(500)
                    .send(new Error('Pet already exists'))
            }
            this.petstore.set(pet.id, pet)
        } catch (caughtError) {
            return reply
                .code(500)
                .send(new Error('Error while adding a new Pet'))
        }
        return reply
            .code(201)
            .send({
                status: 'New pet created'
            })
    }

    getPetById = async (req: FastifyRequest, reply: FastifyReply) => {
        const params: any = req.params
        console.log("get pet from id : " + params.petId)
        const pet = this.petstore.get(params.petId)
        if (pet) {
            return reply.send(pet)
        } else {
            console.error("pet not for id : " + params.petId)
            return reply.callNotFound()
        }
    }

    deletePet = async (req: FastifyRequest, reply: FastifyReply) => {
        const params: any = req.params
        try {
            this.petstore.delete(params.petId)
        } catch (caughtError) {
            return reply
                .code(500)
                .send(new Error(`Error while deleting pet for id ${params.petId}`))
        }
        return reply
            .code(204)
            .send()
    }
}

export default RouteHandler
