import Category from "./Category.js";

export default interface Pet {
    id: number
    name: string
    category: Category
}
