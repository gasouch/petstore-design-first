export default interface Category {
    id: number
    name: string
}

export const dogs:Category = {
    id: 1,
    name: "Dogs"
}

export const fishes:Category = {
    id: 2,
    name: "Fishes"
}
