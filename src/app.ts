import Fastify from 'fastify'
import {dirname, join} from 'path'
import openapiGlue from 'fastify-openapi-glue'
import swagger, {FastifyStaticSwaggerOptions} from '@fastify/swagger'
import RouteHandler from './RouteHandler.js'
import {fileURLToPath} from 'url'
import swaggerUi, { FastifySwaggerUiOptions } from '@fastify/swagger-ui'

const dirName = dirname(fileURLToPath(import.meta.url))
const glueOptions = {
    specification: join(dirName, './petstore.json'),
    service: new RouteHandler()
}

const fastifySwaggerUiOptions: FastifySwaggerUiOptions = {
    routePrefix: `/documentation`
}
const fastifySwaggerOptions: FastifyStaticSwaggerOptions = {
    mode: 'static',
    specification: {
        path: join(dirName, './petstore.json'),
        baseDir: ''
    }
}

const server = Fastify({
    logger: true
})

server.register(openapiGlue, glueOptions)
server.register(swagger, fastifySwaggerOptions)
server.register(swaggerUi, fastifySwaggerUiOptions)

// Run the server!
server.listen({port: 3000, host: '0.0.0.0'}, function (err, address) {
    if (err) {
        server.log.error(err)
        process.exit(1)
    }
    server.log?.info(`Server listening at ${address}`)
})
